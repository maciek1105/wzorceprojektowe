﻿
using MKasprzyk.WzorceProjektowe.Model;

namespace MKasprzyk.WzorceProjektowe.Interfaces
{
    public interface IItemDescriber
    {
        string DescribeBike(Bike bike);
        string DescribePen(Pen pen);
        string DescribeBall(Ball ball);
    }
}
