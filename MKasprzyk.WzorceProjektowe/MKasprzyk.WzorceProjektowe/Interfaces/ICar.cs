﻿using MKasprzyk.WzorceProjektowe.Model;

namespace MKasprzyk.WzorceProjektowe.Interfaces
{
    public interface ICar
    {
        Car DeepCopy(Car source);
        int GetMaxSpeed();
        string ToString();
    }
}