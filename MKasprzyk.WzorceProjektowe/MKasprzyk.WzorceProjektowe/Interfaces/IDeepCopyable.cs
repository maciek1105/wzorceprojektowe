﻿namespace MKasprzyk.WzorceProjektowe.Interfaces
{
    public interface IDeepCopyable<T>
    {
        T DeepCopy(T source);
    }
}