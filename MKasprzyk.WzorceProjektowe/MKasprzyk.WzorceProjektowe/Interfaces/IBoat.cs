﻿namespace MKasprzyk.WzorceProjektowe.Interfaces
{
    public interface IBoat
    {
        int GetMaxSpeed();
    }
}