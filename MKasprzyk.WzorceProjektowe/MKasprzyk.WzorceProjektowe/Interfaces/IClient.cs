﻿using MKasprzyk.WzorceProjektowe.Model;
using System.Collections.Generic;

namespace MKasprzyk.WzorceProjektowe.Interfaces
{
    public interface IClient
    {
        Address Address { get; set; }
        int Age { get; set; }
        List<ICar> Cars { get; set; }
        string Name { get; set; }
        string Surname { get; set; }

        void BuyCar(ICar car);
        void DescribeCars();
        string ToString();
    }
}