﻿using MKasprzyk.WzorceProjektowe.Model;

namespace MKasprzyk.WzorceProjektowe.Adapters
{
    public class CarToVehicleAdapter
    {
        protected readonly Vehicle _vehicle;

        public CarToVehicleAdapter(Car car)
        {
            _vehicle = new Vehicle(car.Color.ToString("f"), $"{car.Brand} {car.Model}", 0);
        }

        public static implicit operator Vehicle(CarToVehicleAdapter adapter) => adapter._vehicle;
    }

}
