﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Command
{
    public interface ICommand
    {
        void Call();
        void Undo();
    }
}
