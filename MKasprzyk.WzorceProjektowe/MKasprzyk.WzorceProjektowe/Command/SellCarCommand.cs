﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Command
{
    public class SellCarCommand : ICommand
    {
        private readonly ICar _car;
        private readonly IClient _client;

        public SellCarCommand(ICar car, IClient client)
        {
            _car = car ?? throw new ArgumentNullException(nameof(car));
            _client = client ?? throw new ArgumentNullException(nameof(client));         
        }

        public void Call()
        {
            try
            {
                _client.BuyCar(_car);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Undo()
        {
            _client.Cars.Remove(_car);
        }
    }
}
