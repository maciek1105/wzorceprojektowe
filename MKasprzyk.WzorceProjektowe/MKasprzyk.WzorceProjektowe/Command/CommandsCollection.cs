﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Command
{
    public class CommandsCollection
    {
        private List<ICommand> _commands;

        public List<ICommand> Commands  { get => _commands ?? (_commands = new List<ICommand>()); set { _commands = value; } }

        public void CallAll()
        {
            _commands.ForEach(x => x.Call());
        }

        public void UndoAll()
        {
            _commands.ForEach(x => x.Undo());
        }
    }
}
