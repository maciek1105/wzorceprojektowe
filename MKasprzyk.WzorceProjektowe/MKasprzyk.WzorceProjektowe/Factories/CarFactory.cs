﻿using MKasprzyk.WzorceProjektowe.Model;
using System;

namespace MKasprzyk.WzorceProjektowe.Factories
{
    public class CarFactory
    {
        public static Car CreateBlackCar(string brand, string model) => new BlackCar( brand, model);

        public static Car CreateRedCar(string brand, string model) => new RedCar(brand, model);
        
    }
}
