﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using MKasprzyk.WzorceProjektowe.Model;
using System;
using System.Collections.Generic;

namespace MKasprzyk.WzorceProjektowe.Builders
{
    public class ClientBuilder
    {
        protected Client _client;

        public ClientBuilder() => _client = new Client();      

        public ClientBuilder SetName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Parameter cannot be null or empty: ", nameof(name));
            }

            _client.Name = name;
            return this;
        }

        public ClientBuilder SetSurname(string sunrame)
        {
            if (string.IsNullOrEmpty(sunrame))
            {
                throw new ArgumentException("Parameter cannot be null or empty: ", nameof(sunrame));
            }

            _client.Surname = sunrame;
            return this;
        }

        public ClientBuilder SetAge(int age)
        {
            _client.Age = age;
            return this;
        }

        public ClientBuilder SetAddress(Address address)
        {
            if (address is null)
            {
                throw new ArgumentNullException(nameof(address));
            }

            _client.Address = address;
            return this;
        }

        public ClientBuilder AddCar(ICar car)
        {
            if (car is null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            if (_client.Cars == null)
            {
                _client.Cars = new List<ICar>();
            }

            _client.Cars.Add(car);
            return this;
        }       

        public static implicit operator Client (ClientBuilder builder) => builder._client;
    }
}
