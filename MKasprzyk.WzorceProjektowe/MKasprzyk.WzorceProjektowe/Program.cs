﻿using MKasprzyk.WzorceProjektowe.Adapters;
using MKasprzyk.WzorceProjektowe.Builders;
using MKasprzyk.WzorceProjektowe.Command;
using MKasprzyk.WzorceProjektowe.Factories;
using MKasprzyk.WzorceProjektowe.Model;
using MKasprzyk.WzorceProjektowe.Model.ItemDescribers;
using System;
using System.Collections.Generic;

namespace MKasprzyk.WzorceProjektowe
{
    class Program
    {

        private static void InformMarket(object sender, MarketTransactionEventArgs eventArgs)
        {
            Console.WriteLine($"Sprzedano {eventArgs.Car}");
        }

        static void Main(string[] args)
        {  
            var clientBuilder = new ClientBuilder();
            var youngClientBuilder = new ClientBuilder();

            var client = (Client)clientBuilder.SetName("Adam").SetSurname("Małysz").SetAge(35);
            var youngClient = (Client)youngClientBuilder.SetName("Adam").SetSurname("Małysz").SetAge(10);

            client.MarketTransaction += InformMarket;
            youngClient.MarketTransaction += InformMarket;

            var blackCar = CarFactory.CreateBlackCar("Subaru", "Impreza");

            client.Cars.Add(blackCar);

            //Console.WriteLine(client.ToString());
            //client.DescribeCars();

            //var vehicleDescripton = VehicleDescriber.Describe(new CarToVehicleAdapter(blackCar));

            //Console.WriteLine(vehicleDescripton);

            var generalDescriber = new GeneralItemDescriber();
            var detailedDescriber = new DetailedItemDescriber();        

            var generalDescribedItems = new List<Item> { new Pen(generalDescriber), new Bike(generalDescriber), new Ball(generalDescriber) } ;
            var detailedDescribedItems = new List<Item> { new Pen(detailedDescriber), new Bike(detailedDescriber), new Ball(detailedDescriber) };

            //foreach(var item in generalDescribedItems)
            //{
            //    Console.WriteLine(item.Describe());
            //}

            //foreach (var item in detailedDescribedItems)
            //{
            //    Console.WriteLine(item.Describe());
            //}

            var clientProxy = new ClientProxy(youngClient);
            //var youngClientProxy = new ClientProxy(youngClient);

            //clientProxy.BuyCar(new BlackCar("Ford", "Mustang"));

            //var sealer = new Sealer();

            //sealer.Seal(new RedCar("Ford", "Mustang"), clientProxy);
            //sealer.Seal(new RedCar("Ford", "Mustang"), youngClientProxy);

            var comandsCollection = new CommandsCollection();
            comandsCollection.Commands.Add(new SellCarCommand(new BlackCar("Deawoo", "Tico"), clientProxy));
            comandsCollection.Commands.Add(new SellCarCommand(new RedCar("Ford", "Focus"), client));

            comandsCollection.CallAll();

            int i = 0;
        }
    }
}
