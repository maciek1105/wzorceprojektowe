﻿using MKasprzyk.WzorceProjektowe.Interfaces;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Boat : IBoat
    {
        private int _displacement;
        private int _maxSpeed;

        public Boat(int displacement, int maxSpeed)
        {
            _displacement = displacement;
            _maxSpeed = maxSpeed;
        }

        public int GetMaxSpeed() => _maxSpeed;
    }
}
