﻿namespace MKasprzyk.WzorceProjektowe.Model
{
    public sealed class RedCar : Car
    {
        public RedCar(string brand, string model) : base(brand, model)
        {
            Color = Enums.Colors.Red;
        }

        public override Car DeepCopy(Car source) => new RedCar(source.Brand, source.Model);
    }
}
