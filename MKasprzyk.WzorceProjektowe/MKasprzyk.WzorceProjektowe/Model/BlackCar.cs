﻿namespace MKasprzyk.WzorceProjektowe.Model
{
    public sealed class BlackCar : Car
    {
        public BlackCar(string brand, string model) : base(brand, model)
        {
            this.Color = Enums.Colors.Black; 
        }

        public override Car DeepCopy(Car source) => new BlackCar(source.Brand, source.Model);
        
    }
}
