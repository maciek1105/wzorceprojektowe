﻿using MKasprzyk.WzorceProjektowe.Interfaces;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Ball : Item
    {
        public Ball(IItemDescriber describer) : base(describer) { }       
        public override string Describe() => _describer.DescribeBall(this);       
    }
}