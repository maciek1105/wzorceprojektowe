﻿using MKasprzyk.WzorceProjektowe.Interfaces;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Bike : Item
    {
        public Bike(IItemDescriber describer) : base(describer) { }       
        public override string Describe() => _describer.DescribeBike(this);
    }
}