﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public abstract class Item
    {
        protected readonly IItemDescriber _describer;
        protected Item(IItemDescriber describer)
        {
            _describer = describer ?? throw new ArgumentNullException(nameof(describer));            
        }

        public Item() { }

        public abstract string Describe();
    }
}
