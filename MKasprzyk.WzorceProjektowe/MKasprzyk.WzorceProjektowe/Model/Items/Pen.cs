﻿using MKasprzyk.WzorceProjektowe.Interfaces;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Pen : Item
    {
        public Pen(IItemDescriber describer) : base(describer) { }
       
        public override string Describe() => _describer.DescribePen(this);
    }
}