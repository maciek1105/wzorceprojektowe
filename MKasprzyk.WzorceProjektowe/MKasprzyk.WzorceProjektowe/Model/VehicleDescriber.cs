﻿using System;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public static class VehicleDescriber
    {
        public static string Describe(Vehicle vehicle) 
            => $"This magnificent {vehicle.Color} vehicle is {vehicle.Type} from {DateTime.Now.Year - vehicle.Age}";
    }
}
