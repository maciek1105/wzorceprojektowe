﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Model.ItemDescribers
{
    public class DetailedItemDescriber : IItemDescriber
    {
        public string DescribeBall(Ball ball)
        {
            return "Look at my ball! My ball is amazing";
        }

        public string DescribeBike(Bike bike)
        {
            return "It's normal boring bike";
        }

        public string DescribePen(Pen pen)
        {
            return "Pencil for writting messages on paper";
        }
    }
}
