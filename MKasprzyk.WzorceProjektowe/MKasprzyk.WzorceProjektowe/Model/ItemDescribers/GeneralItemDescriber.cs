﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Model.ItemDescribers
{
    public class GeneralItemDescriber : IItemDescriber
    {
        public string DescribeBall(Ball ball)
        {
            return ball.GetType().Name;
        }

        public string DescribeBike(Bike bike)
        {
            return bike.GetType().Name;
        }

        public string DescribePen(Pen pen)
        {
            return pen.GetType().Name;
        }
    }
}
