﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Vehicle
    {
        public Vehicle(string color, string type, int age)
        {
            Color = color ?? throw new ArgumentNullException(nameof(color));
            Type = type ?? throw new ArgumentNullException(nameof(type));
            Age = age;
        }

        public string Color { get; set; }
        public string Type { get; set; }
        public int Age { get; set; }
    }
}
