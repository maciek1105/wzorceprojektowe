﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public abstract class Car : IDeepCopyable<Car>, ICar
    {
        protected Car(string brand, string model)
        {
            var radom = new Random();

            Brand = brand ?? throw new ArgumentNullException(nameof(brand));
            Model = model ?? throw new ArgumentNullException(nameof(model));
            _maxSpeed = radom.Next(50, 100);
        }

        public virtual string Brand { get; set; }
        public virtual string Model { get; set; }
        public virtual Enums.Colors Color { get; set; }
        protected int _maxSpeed;

        public virtual Car DeepCopy(Car source)
        {
            throw new NotImplementedException();
        }

        public override string ToString() => $"{Brand} {Model} color: {Color.ToString("f")}";

        public int GetMaxSpeed() => _maxSpeed;
    }
}