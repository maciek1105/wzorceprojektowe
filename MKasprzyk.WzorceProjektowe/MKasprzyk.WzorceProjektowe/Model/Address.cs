﻿using System;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Address
    {
        public Address(string city, string street, string houseNumber, string zip)
        {
            City = string.IsNullOrEmpty(city) == true ? city : throw new ArgumentNullException(nameof(city));
            Street = string.IsNullOrEmpty(street) == true ? street : throw new ArgumentNullException(nameof(street));
            HouseNumber = string.IsNullOrEmpty(houseNumber) == true ? houseNumber : throw new ArgumentNullException(nameof(houseNumber));
            Zip = string.IsNullOrEmpty(zip) == true ? zip : throw new ArgumentNullException(nameof(zip));
        }

        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}