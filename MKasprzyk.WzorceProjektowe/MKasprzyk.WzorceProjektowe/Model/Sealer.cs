﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class Sealer
    {
        private int sealedCars;
        public void Seal(ICar car, IClient client)
        {
            client.BuyCar(car);
            sealedCars++;
        }
    }
}
