﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public sealed class Client : IClient
    {
        private List<ICar> _cars;
        public string Name { get; set; }
        public string Surname { get; set; }
        public Address Address { get; set; }
        public int Age { get; set; }
        public List<ICar> Cars { get => _cars ?? (_cars = new List<ICar>()); set { _cars = value; } }
        public override string ToString() => $"Hello! I'm {Name} {Surname}, age {Age}. I'v got {_cars.Count} car ";
        public void DescribeCars()
        {
            foreach (var car in Cars)
            {
                Console.WriteLine(car.ToString());
            }
        }


        public event EventHandler<MarketTransactionEventArgs> MarketTransaction;

        public void BuyCar(ICar car)
        {
            Cars.Add(car);
            MarketTransaction?.Invoke(this, new MarketTransactionEventArgs { Car = car.ToString()});
        }

        public event PropertyChangedEventHandler PropertyChanged;     
    }
}
