﻿using MKasprzyk.WzorceProjektowe.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKasprzyk.WzorceProjektowe.Model
{
    public class ClientProxy: IClient
    {
        public IClient _client;       

        public ClientProxy(IClient client)
        {
            _client = client;
        }

        public string Name { get => _client.Name; set => _client.Name = value; }
        public string Surname { get => _client.Surname; set => _client.Surname = value; }
        public Address Address { get => _client.Address; set => _client.Address = value; }
        public int Age { get => _client.Age; set => _client.Age = value; }
        public List<ICar> Cars { get => _client.Cars ?? (_client.Cars = new List<ICar>()); set { _client.Cars = value; } }

        public void BuyCar(ICar car)
        {
            if( _client.Age < 18)
            {
                throw new ArgumentException($"Argument Exception: Client {_client.Name} is to young");
            }

            _client.BuyCar(car);
        }

        public void DescribeCars()
        {
            _client.DescribeCars();
        }
    }
}
